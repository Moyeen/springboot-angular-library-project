export class Book {
  id: number | undefined;
  name: string | undefined ;
  author: string | undefined;
}
